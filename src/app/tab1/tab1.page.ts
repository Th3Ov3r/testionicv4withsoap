import { Component } from '@angular/core';
import { NgxSoapService, Client, ISoapMethodResponse } from 'ngx-soap';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  private urlWsdl: any = 'http://186.101.243.202/gmpro/interface.asmx?WSDL';
  private client: Client;
  constructor(private soap: NgxSoapService) {
    // Creo el soap cliente, para esto tengo que darle la ubicación del archivo WSDL
    this.soap.createClient(this.urlWsdl).then(client => this.client = client);
  }

  onClickEvent() {
    console.log(this.client);
  }
}
